package org.mavrick

import io.micronaut.runtime.Micronaut.run

fun main(args: Array<String>) {
    run(*args)
}

